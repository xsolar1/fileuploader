#include <tls.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <unistd.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define READ_BUFF	400 // hopefully enough

char listening[] = {'p','i','n','g'};
char response[] = {'p','o','n','g'};

int main (int argc, char** argv) {
	if (argc != 4 && argc != 3) {
		fprintf(stderr, "Invalid arguments: host port [trusted.pem]\n");
		return EXIT_FAILURE;
	}
	struct tls *tls = tls_client();
	if (!tls) {
		fprintf(stderr, "tls_client\n");
		return EXIT_FAILURE;
	}
	struct tls_config *config = tls_config_new();
	if (!config) {
		perror("tls_config");
		return EXIT_FAILURE;
	}
	if (argc == 4 && tls_config_set_ca_file(config, argv[3]) == -1) {
		fprintf(stderr, "tls_config_set_ca_file: %s\n", tls_config_error(config));
		return EXIT_FAILURE;
	}
	if (tls_configure(tls, config) == -1) {
		fprintf(stderr, "tls configure: %s\n", tls_config_error(config));
		return EXIT_FAILURE;
	} 
	if (tls_connect(tls, argv[1], argv[2]) == -1) {
		perror("tls_connect");
		fprintf(stderr, "tls connect: %s\n", tls_error(tls));
		return EXIT_FAILURE;
	}
	int r;
	if ((r = tls_write(tls, listening, 4)) != 4) {
		fprintf(stderr, "write fail: %s\n", tls_error(tls));
		return EXIT_FAILURE;
	} 
	char buff[READ_BUFF];
	r = tls_read(tls, buff, READ_BUFF);
	if (r < 0) {
		fprintf(stderr, "read fail: %s\n", tls_error(tls));
		return EXIT_FAILURE;
	}
	buff[r] = '\0';
	printf("Subject: %s\n", tls_peer_cert_subject(tls));
	printf("Issuer: %s\n", tls_peer_cert_issuer(tls)); 
	printf("Cipher: %s\n", tls_conn_cipher(tls));
	printf("Server response: '%s'\n", buff);
	tls_config_free(config);
	tls_close(tls);
	tls_free(tls);
	return 0;
}
