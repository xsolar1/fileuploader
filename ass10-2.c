#include <stdlib.h>
#include <openssl/dh.h>
#include <openssl/asn1t.h>

int main(void) {
	DH *dh = DH_new();
	if (!dh) {
		fprintf(stderr, "dh_new fail\n");
		return EXIT_FAILURE;
	}
	if (DH_generate_parameters_ex(dh, 2048, 2, NULL) == 1) {
		int codes = 0;
		int ret = DH_check(dh, &codes);
		if (codes || !ret){
			fprintf(stderr, "dh generate fail\n");
			return EXIT_FAILURE;
		}
	}
	int status = EXIT_FAILURE;
	//i2d_DHparams_bio(dh,dh);
	//i2d_DHparams_fp(stdout, dh);
	//DHparams_print_fp(stdout, dh);
	//printf("%s\n%lu\n\n", SSLeay_version(SSLEAY_VERSION), SSLeay());
	unsigned char *der = NULL;
	int len = i2d_DHparams(dh, &der);
	FILE *fder = fopen("dhparameters.der", "wb");
	if (!fder) {
		perror("foper");
		goto free_all;
	}
	if (fwrite(der, len, 1, fder) != 1) {
		perror("fwrite");
		goto free_all;
	}
	status = EXIT_SUCCESS;
free_all:
	fclose(fder);
	OPENSSL_free(der);
	DH_free(dh);
	return status;
}
