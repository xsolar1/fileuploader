/* OpenSSL example to use TLS connection, cert validation and trivial communication. */

#include <openssl/ssl.h>
#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/pem.h>
#include <openssl/x509.h>

static void handleErrors(void)
{
	ERR_print_errors_fp(stderr);
	abort();
}

void print_cn_name(const char* label, X509_NAME* const name)
{
	int idx = -1;
	unsigned char *utf8 = NULL;
	X509_NAME_ENTRY* entry;
	ASN1_STRING* data;

	if (!name)
		return;

	idx = X509_NAME_get_index_by_NID(name, NID_commonName, -1);
	if (idx < 0)
		return;

	entry = X509_NAME_get_entry(name, idx);
	if (!entry)
		return;

	data = X509_NAME_ENTRY_get_data(entry);
	if (!data)
		return;

	if (!ASN1_STRING_to_UTF8(&utf8, data) || !utf8)
		return;

	printf("%s: %s\n", label, utf8);
	OPENSSL_free(utf8);
}

int verify_callback(int preverify, X509_STORE_CTX* x509_ctx)
{
	X509* cert = X509_STORE_CTX_get_current_cert(x509_ctx);

	if (cert) {
		printf("Cert depth %d\n", X509_STORE_CTX_get_error_depth(x509_ctx));
		print_cn_name("  Issuer", X509_get_issuer_name(cert));
		print_cn_name("  Subject", X509_get_subject_name(cert));
        X509_print_ex_fp(stdout, cert, XN_FLAG_COMPAT, X509_FLAG_COMPAT);
	}

	return preverify;
}

int main(int arc, char **argv)
{
	BIO *bio;
	SSL_CTX *ctx;
	SSL *ssl;
	X509* cert;
	int i;

	SSL_load_error_strings();
	ERR_load_crypto_strings();
	OpenSSL_add_all_algorithms();
	SSL_library_init();

#if OPENSSL_VERSION_NUMBER < 0x10100000L
	ctx = SSL_CTX_new(TLSv1_2_client_method());
#else
	ctx = SSL_CTX_new(TLS_client_method()); 
#endif
	if (!ctx)
	    handleErrors();

	/* Set peer verification mode and callback, used only for print cert. */
	//SSL_CTX_set_verify(ctx, SSL_VERIFY_NONE, NULL);
	SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER, verify_callback);
	SSL_CTX_set_verify_depth(ctx, 4);

	/* USe default certs installed in system (CA certificates) */
	if (SSL_CTX_set_default_verify_paths(ctx) != 1)
		handleErrors();

	bio = BIO_new_ssl_connect(ctx);
	if (!bio)
		handleErrors();

	if (BIO_set_conn_hostname(bio, "www.fi.muni.cz:https") != 1)
		handleErrors();

	BIO_get_ssl(bio, &ssl);
	if (!ssl)
		handleErrors();

	if (SSL_set_tlsext_host_name(ssl, "www.fi.muni.cz") != 1)
		handleErrors();

	SSL_set_mode(ssl, SSL_MODE_AUTO_RETRY);

	if (BIO_do_connect(bio) <= 0)
		handleErrors();

	if (BIO_do_handshake(bio) != 1)
		handleErrors();

	/* Check that we get cert */
	cert = SSL_get_peer_certificate(ssl);
	if (!cert)
		handleErrors();

	/* This is host certificate */
	//print_cn_name("Issuer", X509_get_issuer_name(cert));
	//print_cn_name("Subject", X509_get_subject_name(cert));
	//X509_print_ex_fp(stdout, cert, XN_FLAG_COMPAT, X509_FLAG_COMPAT);

	X509_free(cert);

	/* Should fail if some cert in chain is missing. */
	i = SSL_get_verify_result(ssl);
	if (i != X509_V_OK) {
		printf("Certificate verification error: %i\n", i);
		handleErrors();
	}

	/* HERE SHOULD THE HOMEWORK END */

#ifdef NO_HW
	char buffer[4096];
    int len;
	/* Send GET command for some file (here /robots.txt) through TLS connection */
	if (BIO_puts(bio, "GET /robots.txt HTTP/1.1\nConnection: Close\n\n") <= 0) {
		BIO_free_all(bio);
		handleErrors();
	}

	/* Receive file content */
	while (1) {
		len = BIO_read(bio, buffer, sizeof(buffer)-1);
		if (len == 0)
			break;
		if (len < 0 && !BIO_should_retry(bio)) {
			BIO_free_all(bio);
			handleErrors();
		}
		buffer[len] = 0;
		printf("%s", buffer);
        }
#endif // NO_HW

	BIO_free_all(bio);
	SSL_CTX_free(ctx);

	return 0;
}
