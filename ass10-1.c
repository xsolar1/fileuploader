#include <openssl/sha.h>
#include <stdbool.h>
#include <openssl/evp.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

unsigned char *T = (unsigned char*) "\x30\x21\x30\x09\x06\x05\x2b\x0e\x03\x02\x1a\x05\x00\x04\x14";
unsigned T_len = 15;

bool call_SHA1(void* input, unsigned long length, unsigned char* md)
{
    SHA_CTX context;
    if(!SHA1_Init(&context))
        return false;

    if(!SHA1_Update(&context, (unsigned char*)input, length))
        return false;

    if(!SHA1_Final(md, &context))
        return false;

    return true;
}

unsigned char *hash_file(const char *file) 
{
	FILE *fp = fopen(file, "r");
	if (!fp) {
		perror("fopen");
		exit(EXIT_FAILURE);
	}
	fseek(fp, 0L, SEEK_END);
	unsigned long sz = ftell(fp);
	rewind(fp);
	unsigned char *input = malloc(sz);
	if (!input) {
		perror("malloc");
		exit(EXIT_FAILURE);
	}
	if (sz != fread(input, 1, sz, fp)) {
		perror("fread");
		exit(EXIT_FAILURE);
	}
	unsigned char *hash = malloc(SHA_DIGEST_LENGTH);
	if (!hash) {
		perror("malloc");
		exit(EXIT_FAILURE);
	}
	call_SHA1(input, sz, hash);
	free(input);
	return hash; 
}

void print_hex(void *s, unsigned len)
{
  for (unsigned i = 0; i < len; i++) 
    printf("%02x", (unsigned int) *((unsigned char *) s + i));
  printf("\n");
}

unsigned get_ps_len(unsigned pad_len, unsigned hash_len) {
	int ret = pad_len - hash_len - T_len - 3;
	if (ret < 8) 
		return 8;
	return (unsigned) ret; 
} 

int main (int argc, char **argv) 
{
	if (argc != 3) {
		fprintf(stderr, "Incorrect usage: ./program [file to hash] [pad length]\n");
		return EXIT_FAILURE;
	}
	char *ptr;
	int proc = strtol(argv[2], &ptr, 0);
	if (proc < T_len + SHA_DIGEST_LENGTH + 11 || *ptr != '\0') {
		fprintf(stderr, "Invalid (not an integer  or too small) pad length\n");
		return EXIT_FAILURE;
	}
	unsigned pad_len = (unsigned) proc;
	OpenSSL_add_all_digests();
	unsigned char* hash = hash_file(argv[1]);
	unsigned ps_len = get_ps_len(pad_len, SHA_DIGEST_LENGTH);
	unsigned char *pad = malloc(pad_len);
	if (!pad) {
		perror("malloc");
		return EXIT_FAILURE;
	}
	// set prefix
	*pad = 0;
	*(pad+1) = 1;
	for (unsigned i = 0; i < ps_len; i++)
		*(pad+2+i) = 255;
	*(pad+2+ps_len) = 0;
	memcpy(pad+3+ps_len, T, T_len);
	memcpy(pad+3+ps_len+T_len, hash, SHA_DIGEST_LENGTH);
	print_hex(pad, pad_len);
	EVP_cleanup();
	return 0;
}
