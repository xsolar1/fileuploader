#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#define BPB 16 // bytes per block

/* Never ever hardcode secure keys in code ;-) */
const unsigned char *KEY = (const unsigned char*)"\x00\x11\x22\x33\x44\x55\x66\x77\x88\x99\xaa\xbb\xcc\xdd\xee\xff";
                                                

/* And also IV (Initialization vector) should be generated randomly... */
const unsigned char *IV  = (const unsigned char*)"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01";

static int decrypt(const unsigned char *ciphertext, unsigned char *plaintext, int ciphertext_len)
{
    printf("ciphertext len: %d\n", ciphertext_len);
	EVP_CIPHER_CTX *ctx;
	int len;
	int plaintext_len;

	if (!(ctx = EVP_CIPHER_CTX_new())) {
        fprintf(stderr, "EVP_CIPHER_CTX_new failure\n");
		return EXIT_FAILURE;
    }

	if (EVP_DecryptInit_ex(ctx, EVP_aes_128_cbc(), NULL, KEY, IV) != 1){
        fprintf(stderr, "EVP_DecryptInit_ex failure\n");
		return EXIT_FAILURE;
    }
    
    if (EVP_CIPHER_CTX_set_padding(ctx, 0) != 1) {
        fprintf(stderr, "EVP_CIPHER_CTX_set_padding failure\n");
        return EXIT_FAILURE;
    }

	if (EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext, ciphertext_len) != 1){ 
        fprintf(stderr, "EVP_DecryptUpdate failure\n");
        return EXIT_FAILURE;
    }

	plaintext_len = len;

	if (EVP_DecryptFinal_ex(ctx, plaintext + len, &len) != 1) {
        fprintf(stderr, "EVP_DecryptFinal_ex failure\n");
        ERR_print_errors_fp(stderr);
        return EXIT_FAILURE;
    }

	plaintext_len += len;

	EVP_CIPHER_CTX_free(ctx);

	return EXIT_SUCCESS;
}

static int encrypt(const unsigned char *plaintext, unsigned char *ciphertext, int plaintext_len)
{
 	EVP_CIPHER_CTX *ctx;
	int len;
	int ciphertext_len;

	/* Create and initialise the context */
	if (!(ctx = EVP_CIPHER_CTX_new())) {
        fprintf(stderr, "EVP_CIPHER_CTX_new failure\n");
		return EXIT_FAILURE;
    }

	/* Initialise the encryption operation. IMPORTANT - ensure you use a key
	 * and IV size appropriate for your cipher
	 * In this example we are using 256 bit AES (i.e. a 256 bit key). The
	 * IV size for *most* modes is the same as the block size. For AES this
	 * is 128 bits */
	if (EVP_EncryptInit_ex(ctx, EVP_aes_128_cbc(), NULL, KEY, IV) != 1){
        fprintf(stderr, "EVP_EncryptInit_ex failure\n");
		return EXIT_FAILURE;
    }
    
    if (EVP_CIPHER_CTX_set_padding(ctx, 0) != 1) {
        fprintf(stderr, "EVP_CIPHER_CTX_set_padding failure\n");
        return EXIT_FAILURE;
    }

	/* Provide the message to be encrypted, and obtain the encrypted output.
	 * EVP_EncryptUpdate can be called multiple times if necessary
	 */
	if (EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len) != 1) {
        fprintf(stderr, "EVP_EncryptUpdate failure\n");
        return EXIT_FAILURE;
    }

	ciphertext_len = len;

	/* Finalise the encryption. Further ciphertext bytes may be written at this stage. */
	if (EVP_EncryptFinal_ex(ctx, ciphertext + len, &len) != 1) {
        fprintf(stderr, "EVP_EncryptFinal_ex failure\n");
        return EXIT_FAILURE;
    }

	ciphertext_len += len;

	EVP_CIPHER_CTX_free(ctx);

	return EXIT_SUCCESS;
}

int process_and_encrypt (FILE *in, FILE *out, int enc) {
    fseek(in, 0L, SEEK_END);
    int size = ftell(in);
    rewind(in);
    
    int status = EXIT_FAILURE;
    
    // input file buffering
    unsigned char *buffer_in = malloc(sizeof(char)*size);
    if (!buffer_in) {
        fprintf(stderr, "buffer for input file malloc failure\n");
        return EXIT_FAILURE;
    }
    if (fread(buffer_in, sizeof(char), size, in) != size) {
        fprintf(stderr, "fread didn't read the input file properly. Ending.\n");
        free(buffer_in);
        return EXIT_FAILURE;
    }
    
    // output file buffer
    unsigned char *buffer_out = malloc(sizeof(char)*size);
    if (!buffer_out) {
        fprintf(stderr, "buffer for output file malloc failure\n");
        free(buffer_in);
        return EXIT_FAILURE;
    }
    
    if (enc)
        status = encrypt(buffer_in, buffer_out, size);
    else
        status = decrypt(buffer_in, buffer_out, size);
    
    if (status == EXIT_SUCCESS) {
        if (fwrite(buffer_out, sizeof(char), size, out) != size) {
            fprintf(stderr, "fwrite failure\n");
            status = EXIT_FAILURE;
        }
    }
        
    free(buffer_in);
    free(buffer_out);
    return status;
}


int main (int argc, char **argv) {
    char *ifname = NULL, *ofname = NULL;
    int enc = -1;
    int opt;
    while ((opt = getopt(argc, argv, "i:o:ed")) != -1) {
        switch (opt) {
        case 'i':
            ifname = optarg;
            break;
        case 'o':
            ofname = optarg;
            break;
        case 'e':
            if (enc != -1) {
                fprintf(stderr, "Invalid flag usage.\n");
                return EXIT_FAILURE;
            }
            enc = 1;
            break;
        case 'd':
            if (enc != -1) {
                fprintf(stderr, "Invalid flag usage.\n");
                return EXIT_FAILURE;
            }
            enc = 0;
            break;
        default:
            fprintf(stderr, "Invalid flag.\n");
            return EXIT_FAILURE;
        }
    }
    
    if (enc == -1) {
        fprintf(stderr, "You must include either -e or -d flag.\n");
        return EXIT_FAILURE;
    }
    if (!ifname || !ofname) {
        fprintf(stderr, "Both -i and -o flags are mandatory.\n");
        return EXIT_FAILURE;
    }
    
    /* Initialise the library */
	ERR_load_crypto_strings();
	OpenSSL_add_all_algorithms();
    
    // open the files and check the descriptors
    FILE *in = fopen(ifname, "rb");
    if (!in) {
        perror("Input file not opened correctly");
        return EXIT_FAILURE;
    }
    FILE *out = fopen(ofname, "wb");
    if (!out) {
        perror("Output file not opened correctly");
        return EXIT_FAILURE;
    }
    
    int status = EXIT_FAILURE;
    // make sure the input file is aligned correctly
    fseek(in, 0L, SEEK_END);
    if (ftell(in) % BPB != 0) {
        fprintf(stderr, "Input file incorrectly aligned.\n");
        goto alignment_fail;
    }
    rewind(in);
    
    status = process_and_encrypt(in, out, enc);
    
alignment_fail:
    // close the files and free memory
    fclose(in);
    fclose(out);
    
    return status;
}
