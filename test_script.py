import pickle
# this should work for any density matrix rho_complete (or pure state phi) to the maxially entangled state
import cvxpy as cp
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
sns.set()


def density_pure(phi):
    phi = np.array(phi).reshape((1, len(phi)))
    rho = np.matmul(phi.transpose(), phi) 
    return rho

# q represents noise
def add_white_noise(rho, q):
    n_sq = rho.shape[0]
    return (1-q)*rho + q*np.identity(n_sq)/n_sq

def get_symmetric_density_submatrix(rho):
    d_sq = len(rho)
    d = int(d_sq**0.5)
    sym = []
    for i in range(d):
        row = []
        for j in range(d):
            row.append(rho[i*d + i][j*d + j])
        sym.append(np.array(row))
    return np.array(sym)

def get_maxent(n):
    vec = []
    for i in range(n):
        for j in range(n):
            if i == j:
                vec.append(1)
            else:
                vec.append(0)
    vec = np.array(vec)
    vec = vec/np.linalg.norm(vec)
    return vec

def create_mineof_problem(rho, diagonals):
    n = rho.shape[0]
    X = cp.Variable((n,n), symmetric=True)
    constraints = [X >> 0, cp.trace(X) <= 1]
    for diag in diagonals:
        for i in range(n - diag):
            constraints += [X[diag + i][i] == rho[diag + i][i]]
    prob = cp.Problem(cp.Minimize(cp.sum(cp.abs(X))), constraints)
    return prob

# q represents noise, n is dimensions of the space (only non zero taken)
def compute_eof_diags_with_noise(n, q, diagonals, print_sol=False, prec=1e-5, **args):
    rho = density_pure(get_maxent(n))
    rho = add_white_noise(rho, q)
    rho = get_symmetric_density_submatrix(rho)
    prob = create_mineof_problem(rho, diagonals)
    obj = prob.solve(eps=prec, **args)
    if print_sol:
        print(prob.variables()[0].value)
    B = (obj-1)/np.sqrt(n*(n-1)/2)
    return -np.log2(1-B*B/2)

dall_eof_corr = {}
xs = np.geomspace(1e-3, 1, num=50)
print("computing all diagonal pair combinations")
for n in range(24,31):
    print(f"n=\t{n}")
    if n not in dall_eof_corr:
        dall_eof_corr[n] = {}
    for (i,j) in [(i,j) for i in range(n) for j in range(i)]:
        if (i,j) in dall_eof_corr[n] or (j,i) in dall_eof_corr[n]:
            continue
        ys = []
        for x in xs:
            ys.append(compute_eof_diags_with_noise(n, x, [i,j], prec=1e-5, print_sol=False, max_iters=2**20))
        dall_eof_corr[n][(i,j)] = ys
        dall_eof_corr[n][(j,i)] = ys
    with open('all_diag_pairs_around_kbest_dall_eof_coor_24_30.pkl', 'wb') as handle:
        pickle.dump(dall_eof_corr, handle, protocol=pickle.HIGHEST_PROTOCOL)

