#include <gnutls/abstract.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <gnutls/crypto.h>

/* Never ever hardcode secure keys in code ;-) */
//const unsigned char *KEY = (const unsigned char*)"\x00\x11\x22\x33\x44\x55\x66\x77\x88\x99\xaa\xbb\xcc\xdd\xee\xff";
const gnutls_datum_t KEY = {		       
    (unsigned char *) "\x00\x11\x22\x33\x44\x55\x66\x77"
    "\x88\x99\xaa\xbb\xcc\xdd\xee\xff",
    16
};
                                                

/* And also IV (Initialization vector) should be generated randomly... */
//const unsigned char *IV  = (const unsigned char*)"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01";
const gnutls_datum_t IV = {		       
    (unsigned char*) "\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x01",
    16
};


int process_and_encrypt (FILE *in, FILE *out, int enc, gnutls_cipher_hd_t handle) {
    fseek(in, 0L, SEEK_END);
    int size = ftell(in);
    rewind(in);
    
    int status = EXIT_FAILURE;
    
    // input file buffering
    unsigned char *buffer_in = malloc(sizeof(char)*size);
    if (!buffer_in) {
        fprintf(stderr, "buffer for input file malloc failure\n");
        return EXIT_FAILURE;
    }
    if (fread(buffer_in, sizeof(char), size, in) != size) {
        fprintf(stderr, "fread didn't read the input file properly. Ending.\n");
        free(buffer_in);
        return EXIT_FAILURE;
    }
    
    // output file buffer
    unsigned char *buffer_out = malloc(sizeof(char)*size);
    if (!buffer_out) {
        fprintf(stderr, "buffer for output file malloc failure\n");
        free(buffer_in);
        return EXIT_FAILURE;
    }

	int ret;
    if (enc)
        ret = gnutls_cipher_encrypt2(handle, buffer_in, size, buffer_out, size);
    else
        ret = gnutls_cipher_decrypt2(handle, buffer_in, size, buffer_out, size);
    
	if (ret != 0) {
		fprintf(stderr, "Encryption/decryption failure.\n");
	} else {
        if (fwrite(buffer_out, sizeof(char), size, out) != size) {
            fprintf(stderr, "fwrite failure\n");
            status = EXIT_FAILURE;
        }
		status = EXIT_SUCCESS;
    }
        
    free(buffer_in);
    free(buffer_out);
    return status;
}


int main (int argc, char **argv) {
    char *ifname = NULL, *ofname = NULL;
    int enc = -1;
    int opt;
    while ((opt = getopt(argc, argv, "i:o:ed")) != -1) {
        switch (opt) {
        case 'i':
            ifname = optarg;
            break;
        case 'o':
            ofname = optarg;
            break;
        case 'e':
            if (enc != -1) {
                fprintf(stderr, "Invalid flag usage.\n");
                return EXIT_FAILURE;
            }
            enc = 1;
            break;
        case 'd':
            if (enc != -1) {
                fprintf(stderr, "Invalid flag usage.\n");
                return EXIT_FAILURE;
            }
            enc = 0;
            break;
        default:
            fprintf(stderr, "Invalid flag.\n");
            return EXIT_FAILURE;
        }
    }
    
    if (enc == -1) {
        fprintf(stderr, "You must include either -e or -d flag.\n");
        return EXIT_FAILURE;
    }
    if (!ifname || !ofname) {
        fprintf(stderr, "Both -i and -o flags are mandatory.\n");
        return EXIT_FAILURE;
    }
    
    
    // open the files and check the descriptors
    FILE *in = fopen(ifname, "rb");
    if (!in) {
        perror("Input file not opened correctly");
        return EXIT_FAILURE;
    }
    FILE *out = fopen(ofname, "wb");
    if (!out) {
        perror("Output file not opened correctly");
        return EXIT_FAILURE;
    }
	
	int BPB = gnutls_cipher_get_block_size(GNUTLS_CIPHER_AES_128_CBC);
    
    int status = EXIT_FAILURE;
    // make sure the input file is aligned correctly
    fseek(in, 0L, SEEK_END);
    if (ftell(in) % BPB != 0) {
        fprintf(stderr, "Input file incorrectly aligned.\n");
        goto alignment_fail;
    }
    rewind(in);
	
	gnutls_cipher_hd_t handle;
	if (gnutls_cipher_init(&handle, GNUTLS_CIPHER_AES_128_CBC, &KEY, &IV) != 0) {
		fprintf(stderr, "Cipher not initialised.\n");
		goto cipher_init_failure;
	}
    
    status = process_and_encrypt(in, out, enc, handle);
    
	gnutls_cipher_deinit(handle);
	
cipher_init_failure:
alignment_fail:
    // close the files and free memory
    fclose(in);
    fclose(out);
    
    return status;
}
