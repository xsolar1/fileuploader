CFLAGS = -Wall -std=c99 -g
LDFLAGS = -lssl -lcrypto
TARGETS = ass10-1 ass10-2

all: $(TARGETS)

clean:
	$(RM) *.o $(TARGETS)

.PHONY: clean all
