#include <tls.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <unistd.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

char listening[] = {'p','i','n','g'};
char response[] = {'p','o','n','g'};

int main (int argc, char** argv) {
	if (argc != 4) {
		fprintf(stderr, "Invalid arguments: port cert key\n");
		return EXIT_FAILURE;
	}
	char *errptr;
	int port = (int) strtol(argv[1], &errptr, 0);
	if (*errptr != '\0') {
		fprintf(stderr, "Port: not a number\n");
		return EXIT_FAILURE;
	}
	struct tls *tls = tls_server();
	if (!tls) {
		fprintf(stderr, "tls_server\n");
		return EXIT_FAILURE;
	}
	struct tls_config *config = tls_config_new();
	if (!config) {
		fprintf(stderr, "tls_config\n");
		return EXIT_FAILURE;
	}
	if (tls_config_set_cert_file(config, argv[2]) == -1) {
		perror("set_cert_file");
		return EXIT_FAILURE;
	}
	if (tls_config_set_key_file(config, argv[3]) == -1) {
		perror("set_key_file");
		return EXIT_FAILURE;
	}
	if( tls_configure(tls, config) == -1) {
		perror("could not configure");
		return EXIT_FAILURE;
	}
	int sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) {
		perror("socket");
		return EXIT_FAILURE;
	}
	struct sockaddr_in sa;
	sa.sin_family = AF_INET;
	sa.sin_port = htons(port);
	sa.sin_addr.s_addr = INADDR_ANY;
	
	if (bind(sockfd, &sa, sizeof(sa))) {
		perror("bind");
		return EXIT_FAILURE;
	}
	if (listen(sockfd, 5)) {
		perror("listen");
		return EXIT_FAILURE;
	}
	while (1) {
		int fd;
		if ((fd = accept(sockfd, NULL, NULL)) < 0) {
			perror("accept");
			return EXIT_FAILURE;
		}
		struct tls *cctx;
		if (tls_accept_socket(tls, &cctx, fd) == -1) {
			perror("accept_socket");
			return EXIT_FAILURE;
		}
		char buff[4];
		int r = tls_read(cctx, buff, 4);
		if (r == -1) {
			fprintf(stderr, "%s\n", tls_error(cctx));
			return EXIT_FAILURE;
		}
		if (r == 4) {
			if (!memcmp(listening, buff, 4)) {
				if (tls_write(cctx, response, 4) != 4) {
					fprintf(stderr, "%s\n", tls_error(cctx));
					return EXIT_FAILURE;
				}
			}
		}
		tls_close(cctx);
		tls_free(cctx);
		close(fd);
	}
	tls_free(tls);
}
