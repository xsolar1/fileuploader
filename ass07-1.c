#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <stdlib.h>
#include <stdio.h>

int bitlen = 4096;

int main(void) {
    
    ERR_load_PEM_strings();
	OpenSSL_add_all_algorithms();
    
    int status = EXIT_FAILURE;
    
    BIGNUM *bne = BN_new();
    int ret = BN_set_word(bne, RSA_F4);
    if (ret != 1)
        goto bn_set_failure;
    
    RSA *r = RSA_new();
    ret = RSA_generate_key_ex(r, bitlen, bne, NULL);
    if (ret != 1)
        goto rsa_generate_failure;
    
    if (!PEM_write_RSAPrivateKey(stdout, r, NULL, NULL, 0, NULL, NULL)) {
        fprintf(stderr, "PEM_write_RSAPrivateKey failure\n");
        goto write_failure;
    }
    
    if (!PEM_write_RSAPublicKey(stdout, r)) {
        fprintf(stderr, "PEM_write_RSAPublicKey failure\n");
        goto write_failure;
    }
    
    status = EXIT_SUCCESS;

write_failure:
rsa_generate_failure:
    RSA_free(r);
bn_set_failure:
    BN_free(bne);
    return status;
}
